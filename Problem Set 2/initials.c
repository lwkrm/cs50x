#include <stdio.h>
#include <cs50.h>
#include <string.h>


int main(void)
{
    string name = GetString();
    int counter = 1;
    
    for(int i = 0, n = strlen(name); i < n - 1; i++)
        if(name[i] == ' ' && name[i+1] != ' ')
            counter++;
    
    char initials[counter];
    initials[0] = name[0];
    counter = 1;
    
    for(int i = 0, n = strlen(name); i < n - 1; i++)
        if(name[i] == ' ')
        { 
            initials[counter] = name[i + 1];
            counter++;
        }
     
    for(int i = 0, n = sizeof(initials)/sizeof(char); i < n; i++)
        if(initials[i] >= 97 && initials[i] <= 122)
            initials[i] = (int)initials[i] - 32;
        
    printf("%s\n", initials);
    return 0;
}

