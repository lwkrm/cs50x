#include <stdio.h>
#include <cs50.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define LETTERS 26

int main(int argc, char* argv[])
{
    if(argc != 2)
    {    
        printf("Input keyword (as command-line argument), please.\n");
        return 1;
    }
    
    int key = atoi(argv[1]);
    string message = GetString();
    
    if(key > LETTERS)
        key %= LETTERS;
    
    for(int i = 0, n = strlen(message); i < n; i++)
    {
        if(message[i] >= 65 && message[i] <= 90)
        {   
            message[i] += key;
            if(message[i] > 90)
                message[i] = 64 + abs(90 - message[i]);
        }
        
        if(message[i] >= 97 && message[i] <= 122)
        {
            if(message[i] + key <= 122)
                message[i] += key;
            else
                message[i] = 96 + abs(122 - (message[i] + key));
        }
    }
    
    printf("%s\n", message);

    return 0;
}