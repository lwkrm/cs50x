#include <stdio.h>
#include <string.h>
#include <cs50.h>
#include <ctype.h>
#include <math.h>

char caesar(char message, int key);

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        printf("Input keyword (as command-line argument), please.\n");
        return 1;
    } 
    
    for(int i = argc - 1; i < argc; i++)
        for(int j = 0; j < strlen(argv[i]); j++)
            if(!isalpha(argv[i][j]))
            {    
                printf("Keyword must contain only latin letters.\n");
                return 1;
            }
    
    string keyword = argv[1];
    string message = GetString();

    int  n = strlen(keyword);
    int keys[n];
    
    for(int i = 0; i < n; i++)
    {        
        if(isupper(keyword[i]))
            keys[i] = keyword[i] - 65;
        else
            keys[i] = keyword[i] - 97;
    }

    int k = 0;
    for(int i = 0, j = strlen(message); i < j; i++)
    {   
        if(!isalpha(message[i]))
            message[i] = caesar(message[i], 26);
        else
        {
            if(k < n)
            {
                message[i] = caesar(message[i], keys[k]);
                k++;
            }
            if(k >= n)
                k = 0;
        }
    }
    printf("%s\n", message);
    return 0;
}

char caesar(char message, int key)
{
    if(message >= 65 && message <= 90)
    {   
        message += key;
        if(message > 90)
            message = 64 + abs(90 - message);
    }
    
    if(message >= 97 && message <= 122)
    {
        if(message + key <= 122)
            message += key;
        else
            message = 96 + abs(122 - (message + key));
    }
    return message;    
}