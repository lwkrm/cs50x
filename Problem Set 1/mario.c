#include <stdio.h>
#include <cs50.h>

int main(void)
{
    int height, i, j, k;
    
    do
    {
        printf("Height: ");
        height = GetInt();
        
        if(height == 0)
            return 0;
            
    } while(height < 0 || height > 23);
    
    for(i = 0; i < height; i++)
    {
        for(j = height - (i + 1); j > 0; j--)
            printf(" ");
        for(k = 0; k < i + 2 ; k++)
            printf("#");
        printf("\n");
    }
    
    return 0;
}
