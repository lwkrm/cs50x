#include <stdio.h>
#include <cs50.h>

int main(void)
{
    float change;
    int coins, quarter, dime, nickel, penny, int_change;
    
    int_change = -1;
    coins = 0;
    change = -1;
    quarter = 25;
    dime = 10;
    nickel = 5;
    penny = 1;
    
    printf("O hai! ");
    do
    {
        printf("How much charge is owed?\n");
        change = GetFloat();
    }while(change < 0);
    
    int_change = change * 100;
    if(change - (int_change * 0.01) >= 0.005)
        int_change++;
    
    coins += int_change / quarter;
    int_change = int_change % quarter;

    coins += int_change / dime;
    int_change = int_change % dime; 
   
	coins += int_change / nickel;
    int_change = int_change % nickel;

    coins += int_change / penny;
    int_change = int_change % penny;
    
    printf("%i\n", coins);
    
    return 0;
}
