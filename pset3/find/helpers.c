/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <cs50.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    if(n < 0)
        return false;
    
    for(int i = 0; i < n; i++)
    {
        if(values[i] == value)
            return true;
    }
    
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    unsigned int SwapCounter = 0;
    for(int i = 0; i < n - 1; i++)
    {    
        SwapCounter = 0;
        for(int j = i + 1; j < n; j++)
        {
            if(values[i] > values[j])
            {
                int temp = values[i];
                values[i] = values[j];
                values[j] = temp;
                SwapCounter++;
            }
        }
        if(SwapCounter == 0)
            break;
    }
    
    return;
}

int BinSearch(int key, int arr[], int start, int stop)
{
    if(start > stop)
        return -1;

    int middle = start + (stop - start) / 2;

    if(arr[middle] == key)
         return middle;

    if(middle == stop || middle == start)
        return -2;

    if(key > arr[middle])
    {
        start = middle;
        return BinSearch(key, arr, start, stop);
    }

    if(key < arr[middle])
    {
        stop = middle;
        return BinSearch(key, arr, start, stop);
    }
   
   return -99; 
}